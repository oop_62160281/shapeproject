/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author ASUS
 */
public class Square {

    private double r;

    public Square(double r) {
        this.r = r;
    }

    public double squareArea() {
        return r * r;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        if (r <= 0) {
            System.out.println("Error: Radius must more than zero !!!!");
            return;
        }
        this.r = r;
    }

}
