/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author ASUS
 */
public class Rectangle {

    private double height;
    private double width;

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getArea() {
        return this.height * this.width;
    }

    public void display() {
        System.out.println("Rectangle Height = " + this.height);
        System.out.println("Rectangle width = " + this.width);
        System.out.println("Area of Rectangle = " + getArea());

    }

}
