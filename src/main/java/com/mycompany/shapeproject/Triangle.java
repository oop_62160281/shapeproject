/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author ASUS
 */
public class Triangle {
    double a;
    double b;
    final double triangle = a * b / 2;
    
    public  Triangle(double a , double b){
        this.a = a;
        this.b = b;
        
    }
    public  double triangleArea(){
        return a * b / 2;
    }
    public double getR(){
        return a;
    }
    public  double getR_1(){
        return b;
    }
    public  void setR(double a, double b){
        if(a <= 0 && b <= 0){
            System.out.println("Error: Radius must more than zero !!!!");
            return;
        }
        this.a = a;
        this.b = b;
    }
}
